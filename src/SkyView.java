import java.util.Arrays;
import java.util.stream.Collectors;

public class SkyView {
    private double[][] view;

    public SkyView(int numRows, int numCols, double[] scanned) {
        this.view = new double[numRows][numCols];

        for (int i = 0; i < scanned.length; i++) {
            int x = i / numCols;
            int y = i - x * numCols;

            this.view[x][y] = x % 2 == 0
                    ? scanned[x * numCols + y]
                    : scanned[(x + 1) * numCols - y - 1];
        }

        /*
        for (int x = 0; x < numRows; x++) {
            for (int y = 0; y < numCols; y++) {
                if (x % 2 == 0) {
                    this.view[x][y] = scanned[x * numCols + y];
                } else {
                    this.view[x][y] = scanned[(x + 1) * numCols - y - 1];
                }
            }
        }
        */
    }

    public double getAverage(int startRow, int endRow, int startCol, int endCol) {
        double sum = 0;
        int count = 0;

        for (int x = startRow; x <= endRow; x++) {
            for (int y = startCol; y <= endCol; y++) {
                sum = sum + this.view[x][y];
                count++;
            }
        }
        return sum / count;
    }

    @Override
    public String toString() {
        return Arrays.stream(this.view).map(String::valueOf).collect(Collectors.joining(", "));
    }
}
